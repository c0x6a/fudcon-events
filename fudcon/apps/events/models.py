# -*- coding: utf-8 -*-
"""
Models for Events app
"""

from django.db import models


class Event(models.Model):
    """
    Event model
    """
    name = models.CharField(max_length=40, verbose_name='Nombre')
    price = models.FloatField(verbose_name='Costo')
    start_date = models.DateField(verbose_name='Fecha de inicio')
    start_time = models.TimeField(verbose_name='Hora de inicio')
    end_date = models.DateField(verbose_name='Fecha de cierre')
    end_time = models.TimeField(verbose_name='Hora de cierre')
    address = models.CharField(max_length=20, verbose_name=u'Dirección')
    description = models.TextField(verbose_name=u'Descripción')


class Person(models.Model):
    """
    Person model
    """
    name = models.CharField(max_length=40, verbose_name='Nombre(s)')
    last_name = models.CharField(max_length=40, verbose_name='Apellidos')
    email = models.EmailField(verbose_name=u'Correo electrónico')

    def __unicode__(self):
        return '%s' % self.email

    def get_complete_name(self):
        return '%s %s' % (self.name, self.last_name)


class Registry(models.Model):
    """
    Registration model
    """
    registry_code = models.CharField(
        max_length=13, verbose_name=u'Código', primary_key=True)
    event = models.ForeignKey(Event, verbose_name='Evento')
    person = models.ForeignKey(Person, verbose_name='Persona')

    class Meta:
        unique_together = ('event', 'person')

    def save(self):
        """
        Override self method
        """
        # Generate registry code
        self.registry_code = '%d%d%03d%04d' % (
            self.event.start_date.year,
            self.event.start_date.month,
            self.event.id,
            self.person.id)

        super(Registry, self).save()