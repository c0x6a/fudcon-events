# -*- coding: UTF-8 -*-

# 3rd party imports
from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^$', 'apps.events.views.list_events'),
    url(r'^registrar/(\d+)/$', 'apps.events.views.register_person'),
    url(r'^confirmar/(\d+)/$', 'apps.events.views.confirm_register'),
    url(r'^verificar/(\d+)/$', 'apps.events.views.verify_registry'),
    url(r'^verificar/$', 'apps.events.views.verify_a_registry'),
    url(r'^detalle/(\d+)/$', 'apps.events.views.event_detail'),
)