# -*- coding: UTF-8 -*-
from django.contrib import admin
from apps.events.models import Event, Registry, Person


admin.site.register(Event)
admin.site.register(Person)
admin.site.register(Registry)