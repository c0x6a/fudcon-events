# -*- coding: UTF-8 -*-

# Create your views here.
from datetime import datetime

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from apps.events.forms import PersonForm
from apps.events.models import Event, Registry, Person
from apps.events.utils import generate_qr


def list_events(request):
    """
    List all events
    """
    today = datetime.now().date()
    events = Event.objects.filter(start_date__gte=today)
    data = {
        'events': events
    }
    return render_to_response('list_events.html', data)


def register_person(request, event_id):
    """
    Register a new person to the event
    """
    event = Event.objects.get(id=event_id)
    form_person = PersonForm()

    # Validate data
    if request.method == 'POST':
        form_person = PersonForm(request.POST)
        if form_person.is_valid():
            try:
                person = Person.objects.get(email=request.POST.get('email'))
            except Person.DoesNotExist:
                person = form_person.save()

            registry = Registry(event=event,
                                person=person)
            registry.save()
            return redirect('/evento/confirmar/%s/' % registry.registry_code)

    data = {
        'event': event,
        'form_person': form_person,
    }
    return render_to_response('register_person.html', data,
                              context_instance=RequestContext(request))


def confirm_register(request, registry_code):
    """
    Confirm a registry
    """
    registry = Registry.objects.get(registry_code=registry_code)
    qr_url = 'http://localhost:8000/eventos/verificar/%s/' % registry.registry_code
    qr_code = generate_qr(value=qr_url)
    data = {
        'registry': registry,
        'qr_code': qr_code,
    }
    return render_to_response('confirm_register.html', data)


def verify_registry(request, registry_code):
    """
    Verify a registry
    """
    try:
        registry = Registry.objects.get(registry_code=registry_code)
    except Registry.DoesNotExist:
        registry = None
    data = {
        'registry': registry,
    }
    return render_to_response('verify_registry.html', data)


def event_detail(request, event_id):
    """
    View details of an event
    """
    event = Event.objects.get(id=event_id)
    data = {
        'event': event,
    }
    return render_to_response('event_detail.html', data)


def verify_a_registry(request):
    registry_code = request.GET.get('registry_code', '')
    if registry_code:
        return redirect('/evento/verificar/%s/' % registry_code)
    return render_to_response('verify_a_registry.html')