# -*- coding: UTF-8 -*-


def generate_qr(value, size="250x250"):
    return "http://chart.apis.google.com/chart?chs=%s&cht=qr&chl=%s&choe=UTF-8&chld=H|0" % (size, value)