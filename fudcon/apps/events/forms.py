from django.forms import ModelForm
from apps.events.models import Person


class PersonForm(ModelForm):
    class Meta:
        model = Person